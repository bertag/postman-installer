# Postman Installer (and Updater)

This program installs and updates the Postman REST Client (https://www.getpostman.com/) on Linux systems.  The program will be installed to `/opt/postman`, and a `.desktop` shortcut will be added for all users.

## Installation

Installing the `postman-installer` utility is easy.  Simply run one of the following commands (they do the same thing).  Once installed (assuming that `/usr/local/bin` is on your `$PATH`), you can run the installer at any time by calling `postman-installer` from the command line.

### via wget:
```
sudo wget -O /usr/local/bin/postman-installer https://bitbucket.org/bertag/postman-installer/raw/master/postman-installer
```

### via curl:
```
sudo curl -o /usr/local/bin/postman-installer -sSL https://bitbucket.org/bertag/postman-installer/raw/master/postman-installer
```

## Run Directly

Alternatively, you can download and run the `postman-installer` directly as needed without ever installing it:

### via wget:
```
sudo bash -c "$(wget -O - https://bitbucket.org/bertag/postman-installer/raw/master/postman-installer)"
```

### via curl:
```
sudo bash -c "$(curl -sSL https://bitbucket.org/bertag/postman-installer/raw/master/postman-installer)"
```